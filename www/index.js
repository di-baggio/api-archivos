'use strict'
var env = require('node-env-file'); // .env file
env(__dirname + '/env/enviroments.env', { overwrite: true, raise: false });

var app = require('../app')
var port = process.env.PORT || 3000;


app.listen(port, function () {
    console.log(`Servidor corriendo http://localhost:${port}`);
});

