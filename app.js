'use strict'
var express = require('express');
var cors = require('cors');
var app = express();
const routeArchivo = require('./src/routes/archivos');
var corsOptions = {
    optionsSuccessStatus: 200
}

app.use(express.urlencoded({
    extended: false
}));

app.use(express.json());
app.use(cors(corsOptions));

// TODO: habilitar rutas 

app.use('/api', routeArchivo);


app.get('*', function (req, res) {
    res.status(404).json({ message: "Error al procesar esta consulta", error: { status: 404, message: "Lo siento, no encuentro esta ruta" } });
});

module.exports = app;