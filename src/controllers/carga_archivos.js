'use strict';
const connection = require('../config/database').getConection();
const sql = require('mssql');
const fs = require('fs');
const csv = require('csv-parse');
const { data } = require('node-env-file');
var cabecera = [];
var datos_cabecera = [];

async function readFile(parseador, ruta) {
    var records = []
    cabecera = [];
    const parser = fs
        .createReadStream(ruta)
        .pipe(parseador);
    for await (const record of parser) {
        if (cabecera.length <= 0) {
            cabecera = record;
        } else {
            var dato = { datos: {} };
            var datoBruto = {};
            for (var i = 0; i < datos_cabecera.length; i++) {
                datoBruto[`${cabecera[cabecera.indexOf(datos_cabecera[i])]}`] = record[cabecera.indexOf(datos_cabecera[i])];
            }
            const keys = Object.keys(datoBruto);
            for (var j = 0; j < keys.length; j++) {
                const key = keys[j];

                if (key.includes('direccion')) {
                    if (dato.datos['direcciones'] === undefined) { dato.datos['direcciones'] = '' };
                    dato.datos['direcciones'] += ' ' + datoBruto[key];
                }
                if (key.includes('nombre') > 0) {
                    if (dato.datos['nombres'] === undefined) { dato.datos['nombres'] = '' };
                    dato.datos['nombres'] += ' ' + datoBruto[key];
                }
                if (key.includes('apellido') > 0) {
                    if (dato.datos['apellidos'] === undefined) { dato.datos['apellidos'] = '' };
                    dato.datos['apellidos'] += ' ' + datoBruto[key];
                }
                if (key.includes('telefono') > 0) {
                    if (dato.datos['telefonos'] === undefined) { dato.datos['telefonos'] = '' };
                    dato.datos['telefonos'] += ' ' + datoBruto[key];
                }
                if (dato.datos['apellidos'] === undefined && dato.datos['direcciones'] === undefined && dato.datos['nombres'] === undefined && dato.datos['telefonos'] === undefined) {
                    throw Error('Error al leer el archivo, delimitador errado');
                }
            }
            records.push(dato);
        }
    }
    return records
}

async function registrarCliente(req, res) {
    connection.then(async (pool) => {
        try {
            const config = {
                delimiter: req.body.delimitador,//Delimitador, por defecto es la coma ,
                cast: true, // Intentar convertir las cadenas a tipos nativos
                comment: '#' // El carácter con el que comienzan las líneas de los comentarios, en caso de existir
            }
            const parseador = new csv(config);
            const tipo_columna = typeof req.body.columnas;
            datos_cabecera = (tipo_columna != 'object' && tipo_columna != 'undefined') ? JSON.parse(req.body.columnas) : req.body.columnas;
            var errores = [];
            if (req.body.codigo_campania === undefined || req.body.delimitador === undefined || req.body.columnas === undefined) {
                res.status(403).json({ message: 'Validacion', error: { status: 403, message: 'Debe difinir todos los parametros' } });
                return;
            }
            parseador.on('error', function (err) {
                errores.push(err);
                res.status(500).json({
                    message: "Error al leer el archivo",
                    error: { status: 500, message: err.message }
                });
                return err;
            });
            var direccion = (__dirname.indexOf('src\\controllers') > 0) ? __dirname.substring(0, __dirname.indexOf('src\\controllers')) : __dirname;
            var datos = await readFile(parseador, direccion + `/uploads/${req.params.name}`);
            if (datos.length > 0) {
                var request = new sql.Request(pool);
                var query = `insert into clientes(codigo_campania,nombres,apellidos,direcciones,telefonos)
                             select @codigo_campania,dt.nombres,dt.apellidos,dt.direcciones,dt.telefonos
                             from OPENJSON ( @datos )
                             WITH (
                             nombres   varchar(200)   '$.datos.nombres',
                             apellidos   varchar(200)   '$.datos.apellidos',
                             direcciones   varchar(200)   '$.datos.direcciones',
                             telefonos   varchar(200)   '$.datos.telefonos'
                             ) as dt`;

                var request = new sql.Request(pool);
                request.input('codigo_campania', sql.VarChar, req.body.codigo_campania);
                request.input('datos', sql.NVarChar, JSON.stringify(datos));

                request.query(query, function (err, rows) {
                    if (err) {
                        handlerError(res, err);
                        return;
                    } else {

                        const result = rows.recordset;
                        res.status(200).json(
                            {
                                message: "Consulta ejecutada correctamente",
                                error: {},
                                response: {
                                    result: result
                                }
                            }
                        );
                    }
                });
            }

            if (errores.length > 0) {
                return;
            }

        } catch (err) {

            res.status(500).json({
                message: "Error al procesar esta consulta",
                error: {
                    status: 500,
                    message: `Error: ${err.message}`
                },
                response: {}
            });
        };
    });
}
function handlerError(res, error) {
    var mensaje = 'Error interno del servidor';
    if (error['message'] != null) {
        mensaje = error['message'];
    }
    res.status(500).json(
        {
            message: "Error al procesar esta consulta",
            error: {
                status: 500,
                message: mensaje
            },
            response: {}
        }
    )
}
module.exports = {
    registrarCliente
}