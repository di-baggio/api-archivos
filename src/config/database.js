const sql = require('mssql');
var q = require('q');

module.exports = {
    getConection: function () {
        var deferred = q.defer();
        var pool = new sql.ConnectionPool({
            server: 'localhost',
            database: 'db_nueva',
            port: 1433,
            authentication: {
                type: "default",
                options: {
                    userName: 'sa',
                    password: 'master'
                }
            },
            options: {
                database: process.env.DATABASE_NAME,
                encrypt: false
            }
        }, errorConnect => {
            if (errorConnect) {
                deferred.reject({
                    message: "Error al conectar con la base de datos",
                    descripcion: "Error al establecer conexion con la base de datos",
                    error: errorConnect
                })
            } else {
                deferred.resolve(pool);
            }
        })
        return deferred.promise;
    }
}