'use strict'
var express = require('express');
var multer = require('multer');
var mimeTypes = require('mime-types');

const storage = multer.diskStorage({
    destination: './uploads/',
    filename: function (req, file, cb) {
        const tipo_dato = mimeTypes.extension(file.mimetype);
        const nombreArchivo = req.params.name;
        cb("", nombreArchivo);
    }
});
const upload = multer({
    storage: storage
});

var cargarArchivosController = require('../controllers/carga_archivos');

var api = express.Router();

api.post('/archivo/:name', upload.single('archivo'), cargarArchivosController.registrarCliente);
module.exports = api;
